var makanan = [ 
  {
    "gambar" : "image/taro.png",
    "nama" : "Taro Milk Tea",
    "desc" : "consists of real taro for a strong taste with a granular texture along with creamy milk tea",
    "harga" : "25.000"
  },
  {
    "gambar" : "image/time.png",
    "nama" : "Chatime Milk Tea",
    "desc" : "Brewed from premium loose leaf black tea. Delicious and classic. Perfect for new tea fans!",
    "harga" : "23.000"
  },
  {
    "gambar" : "image/berry.png",
    "nama" : "Strawberry Smoothie",
    "desc" : "Our strawberries have a perfect tart and sweet taste with a delicious strawberry touch!",
    "harga" : "21.000"
  },
  {
    "gambar" : "image/thai.png",
    "nama" : "Thai tea Original",
    "desc" : "The tea you use should have a strong aroma and a very strong black tea taste!",
    "harga" : "24.000"
  },
  {
    "gambar" : "image/apple.png",
    "nama" : "Apple Green Tea",
    "desc" : "The refreshing taste of green apple and natural tea is added with sweetened condensed milk",
    "harga" : "23.000"
  },
  {
    "gambar" : "image/sugar.png",
    "nama" : "Brown Sugar Classic Tea Latte",
    "desc" : "Drink milk tea combined with brown sugar, the taste is not too sweet",
    "harga" : "25.000"
  },
  {
    "gambar" : "image/fresh.png",
    "nama" : "Grass Jelly with Fresh Milk",
    "desc" : "A glass of fresh milk mixed with grass jelly. It is suitable for those who do not like sweet",
    "harga" : "21.000"
  },
  {
    "gambar" : "image/mango.png",
    "nama" : "Mango Green Milk Tea",
    "desc" : "The combination of green tea with fresh mango flavor. the taste is not too sweet and a little sour",
    "harga" : "23.000"
  },
  {
    "gambar" : "image/hazelnut.png",
    "nama" : "Hazelnut Chocolate Milk Tea",
    "harga" : "25000",
    "desc" : "combination of rice, chocolatey, and nutty with all in one milk tea. Best of both worlds!"  
  }
];

const jumlahMENU = (array) => {
  const jmlItemUnsur = document.querySelector('.kolom-order h3');
  const jumlahItem = array.reduce((accumulator) => {
    return accumulator + 1;
  }, 0);
  jmlItemUnsur.innerHTML = jumlahItem;
}

const callbackMap = (item, index) =>{
    const elmnt = document.querySelector('#search');

    elmnt.innerHTML += ` <div class="container" style="margin-right:1px;">
                        <div class="row" style="float: left;">
                        <div class="col-sm m-2" >
                        <div class="card m-3" style="width: 19rem;">
                        <img src="${item.gambar}" class="card-img-top" alt="...">
                        <div class="card-body bg-light">
                          <h5 class="card-title">${item.nama}</h5>
                          <p><b>Rp.${item.harga}</b></p><br>
                          <p class="card-text">${item.desc}</p>
                          <i class="fas fa-star text-success"></i>
                          <i class="fas fa-star text-success"></i>
                          <i class="fas fa-star text-success"></i>
                          <i class="fas fa-star text-success"></i><br>
                          <a href="#" class="btn btn-primary">Beli Sekarang</a>
                        </div>
                      </div>
                      </div>
                      </div>
                      </div>`

}

    makanan.map(callbackMap);
    jumlahMENU(makanan);
                    
  const buttonElmnt = document.querySelector('.button-cari');
                    
  buttonElmnt.addEventListener('click', () => {
                    
  const hasilPencarian = makanan.filter((item, index) => {
                        const inputElmnt = document.querySelector('.input-keyword');
                        const namaItem = item.nama.toLowerCase();
                        const keyword = inputElmnt.value.toLowerCase();
                    
                return namaItem.includes(keyword);
  })
                    
  document.querySelector('#search').innerHTML ='';
  hasilPencarian.map(callbackMap);
});