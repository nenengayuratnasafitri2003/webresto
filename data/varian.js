var varian = [
  {
    "gambar" : "image/matcha.png",
    "nama" : "Matcha Tea Latte",
    "desc" : "Smooth, rich, and earthy topped with fresh milk. This popular latte has a stunning green color!",
    "harga" : "24.000"
  },
  {"gambar" : "image/oolong.png",
    "nama" : "Oolong Tea Latte",
    "desc" : "Flavor roasted tea with a sprinkling of warm, frothy milk for a delicious creamy taste",
    "harga" : "24.000"
  },
  {
    "gambar" : "image/jasmine.png",
    "nama" : "Jasmine Green Milk Tea",
    "desc" : "presents a fragrant and soft tea drink, paired with a rich cream of milk tea!",
    "harga" : "22.000"
  },
  {
    "gambar" : "image/berry.png",
    "nama" : "Strawberry Milk Tea",
    "desc" : "Sweet and sour taste combined with sweetened condensed milk gives a fresh and sweet taste",
    "harga" : "23.000"
  },
  {
    "gambar" : "image/Honey.png",
    "nama" : "Honey Milk Tea",
    "desc" : "The sweetness of honey combines with the softness of jasmine green tea flowers!",
    "harga" : "23.000"
  },
  {
    "gambar" : "image/Fruity.png",
    "nama" : "Passion Fruit Green",
    "desc" : "is made with REAL passion fruit seeds which means it's loaded with delicious, refreshing flavour!",
    "harga" : "20.000"
  },
  {
    "gambar" : "image/winter.png",
    "nama" : "Wintermelon Tea Latte",
    "desc" : "Milk tea has a sweet, strong caramel flavor mixed with a hint of sweetness",
    "harga" : "23.000"
  },
{
    "gambar" : "image/red.png",
    "nama" : "Matcha Red Bean",
    "desc" : "Have an earthy taste of matcha green tea with the sweetness of red beans, in this cool mixed drink!",
    "harga" : "25.000"
  },
  {
    "gambar" : "image/taro.png",
    "nama" : "Taro Milk Tea",
    "desc" : "consists of real taro for a strong taste with a granular texture along with creamy milk tea",
    "harga" : "25.000"
  },
  {
    "gambar" : "image/cocoa.png",
    "nama" : "Pure Cocoa",
    "desc" : "Provides a natural and healthy sweet chocolate taste because it comes from the cocoa fruit.",
    "harga" : "25.000"
  },
  {
    "gambar" : "image/time.png",
    "nama" : "Chatime Milk Tea",
    "desc" : "Brewed from premium loose leaf black tea. Delicious and classic. Perfect for new tea fans!",
    "harga" : "23.000"
  },
  {
    "gambar" : "image/berry.png",
    "nama" : "Strawberry Smoothie",
    "desc" : "Our strawberries have a perfect tart and sweet taste with a delicious strawberry touch!",
    "harga" : "21.000"
  },
  {
    "gambar" : "image/tea.png",
    "nama" : "Thai tea Original",
    "desc" : "The tea you use should have a strong aroma and a very strong black tea taste!",
    "harga" : "24.000"
  },
  {
    "gambar" : "image/apple.png",
    "nama" : "Apple Green Tea",
    "desc" : "The refreshing taste of green apple and natural tea is added with sweetened condensed milk",
    "harga" : "23.000"
  },
  {
    "gambar" : "image/sugar.png",
    "nama" : "Brown Sugar Classic Tea Latte",
    "desc" : "Drink milk tea combined with brown sugar, the taste is not too sweet",
    "harga" : "25.000"
  },
  {
    "gambar" : "image/fresh.png",
    "nama" : "Grass Jelly with Fresh Milk",
    "desc" : "A glass of fresh milk mixed with grass jelly. It is suitable for those who do not like sweet",
    "harga" : "21.000"
  },
  {
    "gambar" : "image/mango.png",
    "nama" : "Mango Green Milk Tea",
    "desc" : "The combination of green tea with fresh mango flavor. the taste is not too sweet and a little sour",
    "harga" : "23.000"
  },
  {
    "gambar" : "image/hazelnut.png",
    "nama" : "Hazelnut Chocolate Milk Tea",
    "harga" : "25000",
    "desc" : "combination of rice, chocolatey, and nutty with all in one milk tea. Best of both worlds!"  
  }
];
const jumlahMENU = (array) => {
  const jmlItemUnsur = document.querySelector('.kolom-order h3');
  const jumlahItem = array.reduce((accumulator) => {
    return accumulator + 1;
  }, 0);
  jmlItemUnsur.innerHTML = jumlahItem;
}

const callbackMap = (item, index) =>{
    const elmnt = document.querySelector('#search');

    elmnt.innerHTML +=` <div class="row" style="float: right;">
                                    <div class="col mb-3" >
                                    <div class="card m-2" style="width: 15rem;">
                                <img src="${item.gambar}" class="card-img-top" alt="...">
                                <div class="card-body bg-light">
                                  <h5 class="card-title">${item.nama}</h5>
                                  <p><b>Rp.${item.harga}</b></p><br>
                                  <p class="card-text">${item.desc}</p>
                                  <i class="fas fa-star text-success"></i>
                                  <i class="fas fa-star text-success"></i>
                                  <i class="fas fa-star text-success"></i>
                                  <i class="fas fa-star text-success"></i><br>
                                  <a href="#" class="btn btn-primary">Beli Sekarang</a>
                                </div>
                              </div>
                            </div>
                            </div>
                    </div>`

}
                    varian.map(callbackMap);
                    jumlahMENU(varian);
                    
                    const buttonElmnt = document.querySelector('.button-cari');
                    
                    buttonElmnt.addEventListener('click', () => {
                    
                      const hasilPencarian = varian.filter((item, index) => {
                                            const inputElmnt = document.querySelector('.input-keyword');
                                            const namaItem = item.nama.toLowerCase();
                                            const keyword = inputElmnt.value.toLowerCase();
                    
                                            return namaItem.includes(keyword);
                              })
                    
                              document.querySelector('#search').innerHTML ='';
                              hasilPencarian.map(callbackMap);
                    });