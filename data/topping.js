var bubble = [
  
  {
    "gambar" : "image/coffe.png",
    "nama" : "Coffe Jelly",
    "desc" : "R: 200 Cals, L: 260 Cals. Calories displayed for the drink prepared with normal ice and normal sugar.",
    "harga" : "5.000"
  },
  {
    "gambar" : "image/grass.png",
    "nama" : "Grass Jelly",
    "desc" : "R: 60 Cals, L: 80 Cals. Calories displayed for the drink prepared with normal ice and normal sugar.",
    "harga" : "4.000"
  },
  {
    "gambar" : "image/jelly.png",
    "nama" : "Rainbow Jelly",
    "desc" : "R: 140 Cals, L: 180 Cals. Calories displayed for the drink prepared with normal ice and normal sugar.",
    "harga" : "5.000"
  },
  {
    "gambar" : "image/pudding.png",
    "nama" : "Pudding",
    "desc" : "R: 90 Cals, L: 120 Cals. calories displayed for the drink prepared with normal ice and normal sugar.",
    "harga" : "4.000"
  },
  {
    "gambar" : "image/aloe.png",
    "nama" : "Aloe vera",
    "desc" : "R: 90 Cals, L: 100 Cals. Calories displayed for the drink prepared with normal ice and normal sugar.",
    "harga" : "5.000" 
  },
  {
    "gambar" : "image/red bean.png",
    "nama" : "Red bean",
    "desc" : "R: 260 Cals, L: 290 Cals. Calories displayed for the drink prepared with normal ice and normal sugar.",
    "harga" : "5.000" 
  },
  {
    "gambar" : "image/bubble.png",
    "nama" : "Pearls",
    "desc" : "R: 150 Cals, L: 190 Cals. Calories displayed for the drink prepared with normal ice and normal sugar.",
    "harga" : "4.000"
  },
  {
    "gambar" : "image/coco.png",
    "nama" : "Coconut Jelly",
    "desc" : "R: 130 Cals, L: 170 Cals. Calories displayed for the drink prepared with normal ice and normal sugar.",
    "harga" : "5.000"
  }
];


const jumlahMENU = (array) => {
  const jmlItemUnsur = document.querySelector('.kolom-order h3');
  const jumlahItem = array.reduce((accumulator) => {
    return accumulator + 1;
  }, 0);
  jmlItemUnsur.innerHTML = jumlahItem;
}

const callbackMap = (item, index) =>{
    const elmnt = document.querySelector('#search');

    elmnt.innerHTML += `<div class="row" style="float: left;">
                        <div class="col mb-3" >
                        <div class="card m-3" style="width: 19rem;">
                    <img src="${item.gambar}" class="card-img-top" alt="...">
                    <div class="card-body bg-light">
                      <h5 class="card-title">${item.nama}</h5>
                      <p><b>Rp.${item.harga}</b></p><br>
                      <p class="card-text">${item.desc}</p>
                      <i class="fas fa-star text-success"></i>
                      <i class="fas fa-star text-success"></i>
                      <i class="fas fa-star text-success"></i>
                      <i class="fas fa-star text-success"></i><br>
                      <a href="#" class="btn btn-primary">Beli Sekarang</a>
                    </div>
                    </div>
                    </div>
                    </div>
                    </div>`
}

bubble.map(callbackMap);
jumlahMENU(bubble);

const buttonElmnt = document.querySelector('.button-cari');

buttonElmnt.addEventListener('click', () => {

  const hasilPencarian = bubble.filter((item, index) => {
                        const inputElmnt = document.querySelector('.input-keyword');
                        const namaItem = item.nama.toLowerCase();
                        const keyword = inputElmnt.value.toLowerCase();

                        return namaItem.includes(keyword);
          })

          document.querySelector('#search').innerHTML ='';
          hasilPencarian.map(callbackMap);
});